{ pkgs ? import <nixpkgs> {} }:
let
  result = import (pkgs.fetchFromGitHub {
    owner = "dmjio";
    repo = "miso";
    sha256 = "1axj5226543nc3m4xwx144xb3k9ziwzmb31vwwkinlc2nvm63343";
    rev = "2340394d90420b20a11bd87ce4db7b8537e9384d";
  }) {};
in pkgs.haskell.packages.ghcjs.callPackage ./app.nix {
  miso = result.miso-ghcjs;
}
